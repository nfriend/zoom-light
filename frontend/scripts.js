import 'wired-elements';

const solidRegex = /^solid_(\d+)_(\d+)_(\d+)_(\d+)$/;
const gradientRegex =
  /^gradient_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)$/;
const predefinedColors = ['on', 'off', 'rainbow'];

let gradientColorLeft = '255_0_0_0';
let gradientColorRight = '255_0_0_0';

const isValidColorConfiguration = (config) => {
  return (
    predefinedColors.includes(config) ||
    solidRegex.test(config) ||
    gradientRegex.test(config)
  );
};

const setStatusCardBackgroundToDefault = () => {
  const statusCardEl = document.getElementById('status-card');
  statusCardEl.classList.forEach((c) => {
    if (isValidColorConfiguration(c)) {
      statusCardEl.classList.remove(c);
    }
  });
};

const setStatusCardBackgroundTo = (what) => {
  const statusCardEl = document.getElementById('status-card');

  if (solidRegex.exec(what)) {
    const match = solidRegex.exec(what);
    const r = match[1];
    const g = match[2];
    const b = match[3];
    const a = 1 - parseInt(match[4], 10) / (255 * 1.2);

    statusCardEl.style.background = `rgba(${r}, ${g}, ${b}, ${a})`;
  } else if (gradientRegex.exec(what)) {
    const match = gradientRegex.exec(what);
    const r1 = match[1];
    const g1 = match[2];
    const b1 = match[3];
    const a1 = 1 - parseInt(match[4], 10) / (255 * 1.2);
    const r2 = match[5];
    const g2 = match[6];
    const b2 = match[7];
    const a2 = 1 - parseInt(match[8], 10) / (255 * 1.2);

    statusCardEl.style.background = `linear-gradient(90deg, rgba(${r1},${g1},${b1},${a1}) 0%, rgba(${r2},${g2},${b2},${a2}) 100%)`;
  } else if (predefinedColors.includes(what)) {
    statusCardEl.style.background = null;
    document.getElementById('status-card').classList.add(what);
  } else {
    throw new Error(`Unrecognized color configuration: "${what}"`);
  }
};

const turnLight = async (what) => {
  try {
    setStatusCardBackgroundToDefault();

    const path =
      solidRegex.test(what) || gradientRegex.test(what)
        ? `custom/${what}`
        : what;
    await fetch(`/api/led/${path}`, { method: 'POST' });

    setStatusCardBackgroundTo(what);
  } catch (err) {
    console.error(`An error occurred while turning the light ${what}:`, err);
  }
};

document
  .querySelectorAll('.control-btn:not(#custom-btn):not(#gradient-btn)')
  .forEach((el) => {
    const what = el.getAttribute('data-color-def');
    el.addEventListener('click', async () => {
      turnLight(what);
    });
  });

document.getElementById('custom-btn').addEventListener('click', async () => {
  const colorDef = document.getElementById('color-def').value;

  if (!isValidColorConfiguration(colorDef)) {
    alert(`"${colorDef}" is not a valid color definition string`);
  } else {
    turnLight(colorDef);
  }
});

document.querySelectorAll('.gradient-btn').forEach((el) => {
  el.addEventListener('click', async () => {
    const color = el.getAttribute('data-color-def');
    if ([...el.classList].includes('gradient-btn-left')) {
      gradientColorLeft = color;
    } else {
      gradientColorRight = color;
    }

    turnLight(`gradient_${gradientColorLeft}_${gradientColorRight}`);
  });
});

document
  .getElementById('color-def')
  .addEventListener('keydown', async (event) => {
    if (event.key === 'Enter') {
      document.getElementById('custom-btn').click();
    }
  });

const setInitialStatus = async () => {
  try {
    const response = await fetch(`/api/led`);
    const { status: what } = await response.json();

    setStatusCardBackgroundTo(what);

    // initialize the local gradient color state
    // if the light is currently set to a gradient
    if (gradientRegex.test(what)) {
      const match = gradientRegex.exec(what);
      gradientColorLeft = `${match[1]}_${match[2]}_${match[3]}_${match[4]}`;
      gradientColorRight = `${match[5]}_${match[6]}_${match[7]}_${match[8]}`;
    } else if (solidRegex.test(what)) {
      const match = solidRegex.exec(what);
      gradientColorLeft = `${match[1]}_${match[2]}_${match[3]}_${match[4]}`;
      gradientColorRight = gradientColorLeft;
    } else if (what === 'on') {
      gradientColorLeft = `0_0_0_255`;
      gradientColorRight = gradientColorLeft;
    }
  } catch (err) {
    console.error(
      `An error occurred while getting the current status of the LED strip`,
      err
    );
  }
};

setInitialStatus();
