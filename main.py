import wifi
import re
import os
from machine import Pin
from lib.neopixel import Neopixel
from lib.microdot import Microdot, Response, send_file, redirect

wifi.connect()

board_led = Pin("LED", Pin.OUT)
NUM_PIXELS = 16
pixels = Neopixel(NUM_PIXELS, 0, 0, "GRBW")
led_status = "off"

Response.default_content_type = "application/json"
app = Microdot()


def get_board_light_status_response():
    return {"status": board_led.value() != 0}


def get_led_status_response():
    return {"status": led_status}


@app.route("/api/board/light/on", methods=["POST"])
async def board_light_on(request):
    board_led.value(1)
    return get_board_light_status_response()


@app.route("/api/board/light/off", methods=["POST"])
async def board_light_off(request):
    board_led.value(0)
    return get_board_light_status_response()


@app.route("/api/board/light/toggle", methods=["POST"])
async def board_light_toggle(request):
    board_led.toggle()
    return get_board_light_status_response()


@app.route("/api/board/light")
async def get_board_light_status(request):
    return get_board_light_status_response()


@app.route("/api/led")
async def get_led_strip_status(request):
    return get_led_status_response()


@app.route("/api/led/on", methods=["POST"])
async def led_strip_on(request):
    pixels.clear()
    pixels.fill((0, 0, 0, 255))
    pixels.show()

    global led_status
    led_status = "on"


@app.route("/api/led/rainbow", methods=["POST"])
async def led_strip_rainbow(request):
    pixels.clear()
    for i in range(0, NUM_PIXELS):
        pixels.set_pixel(
            # Reversing since my LED strip is installed backward
            NUM_PIXELS - 1 - i,
            pixels.colorHSV(round((65535.0 / NUM_PIXELS) * i), 255, 255),
        )
    pixels.show()

    global led_status
    led_status = "rainbow"


@app.route("/api/led/off", methods=["POST"])
async def led_strip_off(request):
    pixels.clear()
    pixels.show()

    global led_status
    led_status = "off"


solid_color_match = re.compile(r"^solid_(\d+)_(\d+)_(\d+)_(\d+)$")
gradient_match = re.compile(
    r"^gradient_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+)$"
)


@app.route("/api/led/custom/<color_def>", methods=["POST"])
async def led_strip_custom(request, color_def):
    pixels.clear()

    try:
        if match := solid_color_match.match(color_def):
            color = (
                int(match.group(1)),
                int(match.group(2)),
                int(match.group(3)),
                int(match.group(4)),
            )

            pixels.fill(color)
        elif match := gradient_match.match(color_def):
            color_1 = (
                int(match.group(1)),
                int(match.group(2)),
                int(match.group(3)),
                int(match.group(4)),
            )
            color_2 = (
                int(match.group(5)),
                int(match.group(6)),
                int(match.group(7)),
                int(match.group(8)),
            )

            # Reversing since my LED strip is installed backward
            pixels.set_pixel_line_gradient(0, NUM_PIXELS - 1, color_2, color_1)
        else:
            raise Exception('unrecognized color definition format: "{color_def}"')

        pixels.show()

        global led_status
        led_status = color_def
    except:
        return {"error": 'invalid color definition param: "{color_def}"'}, 400


MAX_AGE = 86400


@app.route("/")
async def frontend(request):
    return send_file("/frontend/dist/index.html", max_age=MAX_AGE)


def file_exists(filename):
    try:
        os.stat(filename)
        return True
    except OSError:
        return False


@app.route("/<path:path>", methods=["GET"])
async def static(request, path):
    if ".." in path:
        # directory traversal is not allowed
        return send_file("/frontend/dist/404.html", max_age=MAX_AGE, status_code=404)

    requested_file = f"/frontend/dist/{path}"

    if file_exists(requested_file):
        print(f"{requested_file} exists, sending...")
        return send_file(requested_file, max_age=MAX_AGE)
    else:
        print(f"{requested_file} does not exist, 404")
        return send_file("/frontend/dist/404.html", max_age=MAX_AGE, status_code=404)


@app.errorhandler(404)
async def not_found(request):
    return {"error": "resource not found"}, 404


app.run(port=80)
