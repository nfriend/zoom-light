#!/usr/bin/env bash

# How often to poll for Zoom status, in seconds
INTERVAL=5

get_api_url() {
    local base_url="http://zoomlight/api/led"

    local current_date
    current_date=$(date +"%m-%d")

    case $current_date in
        # This list generated by ChatGPT
        "01-01")  echo "$base_url/rainbow";; # New Year's Day
        "01-06")  echo "$base_url/custom/solid_255_255_0_0";; # Epiphany
        "01-30")  echo "$base_url/rainbow";; # Franklin D. Roosevelt's Birthday
        "02-02")  echo "$base_url/custom/solid_0_255_0_255";; # Groundhog Day
        "02-07")  echo "$base_url/rainbow";; # Ashton Kutcher's Birthday
        "02-12")  echo "$base_url/custom/solid_255_69_0_0";; # Lincoln's Birthday
        "02-14")  echo "$base_url/custom/solid_255_0_0_0";; # Valentine's Day
        "02-15")  echo "$base_url/custom/solid_0_191_255_0";; # Family Day (Canada)
        "02-22")  echo "$base_url/custom/solid_173_216_230_0";; # Washington's Birthday
        "03-17")  echo "$base_url/custom/solid_0_255_0_0";; # St. Patrick's Day
        "04-01")  echo "$base_url/rainbow";; # April Fool's Day
        "04-02")  echo "$base_url/custom/solid_75_0_130_0";; # Good Friday
        "04-04")  echo "$base_url/custom/solid_255_223_0_0";; # Easter
        "04-22")  echo "$base_url/custom/solid_0_100_0_0";; # Earth Day
        "05-05")  echo "$base_url/custom/solid_255_165_0_0";; # Cinco de Mayo
        "05-09")  echo "$base_url/custom/solid_255_105_180_0";; # Mother's Day
        "05-24")  echo "$base_url/custom/solid_255_215_0_0";; # Victoria Day (Canada)
        "05-31")  echo "$base_url/custom/solid_0_0_255_0";; # Memorial Day
        "06-14")  echo "$base_url/custom/solid_255_0_0_0";; # Flag Day (USA)
        "06-19")  echo "$base_url/rainbow";; # Brandon Lake's Birthday
        "06-20")  echo "$base_url/custom/solid_255_105_180_0";; # Father's Day
        "07-01")  echo "$base_url/custom/solid_255_0_0_0";; # Canada Day
        "07-04")  echo "$base_url/custom/gradient_255_0_0_0_0_0_255_0";; # Independence Day
        "08-02")  echo "$base_url/custom/solid_0_191_255_0";; # Civic Holiday (Canada)
        "09-01")  echo "$base_url/rainbow";; # Zendaya's Birthday
        "09-06")  echo "$base_url/custom/solid_0_0_255_0";; # Labor Day
        "10-10")  echo "$base_url/custom/solid_255_105_180_0";; # Thanksgiving (Canada)
        "10-11")  echo "$base_url/custom/solid_255_165_0_0";; # Columbus Day
        "10-31")  echo "$base_url/custom/gradient_255_165_0_0_0_0_0_0";; # Halloween
        "11-11")  echo "$base_url/custom/solid_255_0_0_0";; # Veterans Day / Remembrance Day
        "11-25")  echo "$base_url/custom/solid_255_223_0_0";; # Thanksgiving (USA)
        "11-26")  echo "$base_url/custom/solid_0_0_255_0";; # Black Friday
        "12-06")  echo "$base_url/custom/solid_255_140_0_0";; # St. Nicholas Day
        "12-21")  echo "$base_url/custom/solid_0_0_255_255";; # Winter Solstice
        "12-24")  echo "$base_url/custom/solid_255_0_0_0";; # Christmas Eve
        "12-25")  echo "$base_url/custom/gradient_0_255_0_0_255_0_0_0";; # Christmas Day
        "12-26")  echo "$base_url/custom/solid_0_0_255_0";; # Boxing Day
        "12-31")  echo "$base_url/rainbow";; # New Year's Eve

        # Additional popular holidays
        "01-20")  echo "$base_url/custom/solid_0_0_139_0";; # Martin Luther King Jr. Day
        "02-17")  echo "$base_url/custom/solid_75_0_130_0";; # Presidents' Day
        "03-14")  echo "$base_url/custom/solid_255_0_255_0";; # Pi Day
        "05-01")  echo "$base_url/custom/solid_255_69_0_0";; # May Day
        "07-24")  echo "$base_url/custom/solid_255_140_0_0";; # Pioneer Day (Utah)
        "08-15")  echo "$base_url/custom/solid_255_105_180_0";; # Assumption of Mary
        "09-17")  echo "$base_url/custom/solid_0_128_128_0";; # Constitution Day
        "10-04")  echo "$base_url/custom/solid_255_165_0_0";; # Yom Kippur
        "10-16")  echo "$base_url/custom/solid_128_0_128_0";; # Boss's Day
        "11-01")  echo "$base_url/custom/solid_255_223_0_0";; # All Saints' Day
        "12-13")  echo "$base_url/custom/solid_255_20_147_0";; # St. Lucia Day
        *)
            echo "$base_url/on";; # Default to white
    esac
}

# Function to execute the command and process its output
monitor_zoom() {
    current_state='unknown'

    while true; do
        # output will be an integer representing the number of open Zoom ports
        output=$(lsof -i 4UDP | grep zoom | awk 'END{print NR}')

        if [[ $output -gt 2 && $current_state != "on" ]]; then
            # In practice, when on a Zoom call, $output seems to always be 6

            current_state="on"
            echo "Turning light on"
            curl -X POST "$(get_api_url)"
        elif [[ $output -le 2 && $current_state != "off" ]]; then
            # In practice, when not on a Zoom call, $output seems to always be 1

            current_state="off"
            echo "Turning light off"
            curl -X POST http://zoomlight/api/led/off
        fi

        sleep $INTERVAL
    done
}

echo "Watching for Zoom meetings in the background with PID: $$"

# Start the monitoring
monitor_zoom
