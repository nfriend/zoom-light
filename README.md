# zoom-light

A 3D-printed light that turns on when a Zoom call is active. Powered by a Raspberry Pi Pico.

<img src="./images/zoom_light_small.webp" alt="An animated image of the Zoom Light in action">

## How does it work?

- A [`bash` command](./scripts/monitor-webcam.sh) running on the machine polls for Zoom call status
- When a meeting is detected, the script makes a network request to `http://zoomlight/api/led/on`
- A webserver running on the wireless Pico receives this request and turns on the LED strip
- When the meeting ends, the script calls `http://zoomlight/api/led/off`, and the Pico turns the strip off

## Build log

https://nathanfriend.io/2024/05/31/zoom-light.html

## What's in this repo?

Source code for the Raspberry Pi Pico webserver that controls the LED strip is in [`main.py`](./main.py).

3D printed models (`.stl`s and [FreeCAD](https://www.freecad.org/) source files) can be found in [`./things`](./things/).

The `bash` script to listen for Zoom meetings is [`./scripts`](./scripts/).

## API

### `GET /api/led`

Gets the current status of the LED strip in a format like:

```json
{ "status": "on" }
```

### `POST /api/led/on`

Turns the light on (full white).

### `POST /api/led/off`

Turns the light off.

### `POST /api/led/rainbow`

Turns the light on with a rainbow gradient.

### `POST /api/led/custom/<color definition>`

Turns the light on with a custom color. `color_definition` must be a string conforming to one of the following two formats:

#### Solid color:

`solid_RRR_GGG_BBB_WWW`

where `RRR`, `GGG`, `BBB`, and `WWW` are integers between 0 and 255 for each color channel, respectively.

#### Gradient:

`gradient_RRR1_GGG1_BBB1_WWW1_RRR2_GGG2_BBB2_WWW2`

Similar to the above, except the first 4 numbers represent the color on the left, and the second the color on the right.

## Developing the Raspberry Pi Pico server

Create an `env.py` file in the root of the project like this:

```py
# Wi-Fi credentials
ssid = 'your WiFi network name here'
password = 'your WiFi password here'
```

Then, inside `frontend`, run:

```
yarn && yarn run build
```

Finally, run `main.py` on the Raspberry Pi Pico. (To do this through VS Code, run the `Upload project` command provided by the [MicroPico extension](https://github.com/paulober/MicroPico).)

### Developing the frontend

Inside [`frontend`](./frontend/) is a small frontend app for displaying the current status of the Zoom light and modifying its state:

<img src="./images/frontend.jpg" width="300">

The background color of the box at the top of the page indicates the current state of the light.

For development:

```sh
cd frontend
yarn
yarn run dev
```

During development, API requests will be proxied to `http://zoomlight/api/...` (see [`.proxyrc`](./frontend/.proxyrc)).

To produce a production build (in `frontend/dist`):

```sh
yarn run build
```

**Important note:** Only file extensions that match the list in `micropico.syncFileTypes` inside [`.vscode/settings.json`](.vscode/settings.json) will be uploaded to the Pico.
